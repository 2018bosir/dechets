Compte rendu 19/11/2018 
Notre application a pour objectif de rassembler des groupes de personne dans l�optique de ramasser des d�chets � un endroit donn�, ces �v�nements sont cr��s par les utilisateurs et appellent d�autres utilisateurs � participer.

Nous avions pour objectif de construire aujourd�hui toutes les classes dont nous avons besoin. Sylvain et Zineb ont construit les classes Evenement, ListeEvenement, Accueil, MonProfilActivity et Inscription, Rapha�l et Paul ont �crit Connexion, Personne, Profil, CreationEvenement, MapsActivity. 
Nous nous occuperons demain de lier ces activit�s entre elles et de les tester pour des bases de donn�es cr��es par nous m�me avant de g�n�raliser au cas principal.
Liste des classes : 
	-Accueil
	-Connexion
	-CreationEvenement
	-Evenement
	-InscriptionActivity
	-listeEvenement
	-MenuPrincipalActivity
	-Personne
	-Profils
	-MapsActivity
	-Afficher EvACtivity
	-MonProfilActivity
Objectifs :
	Etre capable de selectionner le lieux � nettoyer gr�ce � la carte GoogleMaps
	Permettre aux participants de rejoindre un �venement deja cr��
	Permettre au cr�ateur de l'�v�nement de d�finir un nombre limite de participants
	R�aliser un affichage des �venements en cours ergonomique

	Eventuellement : 
	Stocker les donn�es sur un serveur web
	