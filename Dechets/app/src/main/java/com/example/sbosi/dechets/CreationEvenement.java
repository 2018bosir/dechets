package com.example.sbosi.dechets;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.Date;


public class CreationEvenement extends AppCompatActivity {

    private Vector<Double> lieu;
    private String datestr;
    private int impNetoy;  //entier de 0 a 3
    private int nbrPartSouhaité;
    private Vector<Integer> listeParticipant;
    private Date date;
    private String description;
    private int id;
    private EditText Edannee;
    private EditText Edmois;
    private EditText Edjour;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creation_evenement);



        //affectation des variables pour l'évènement en cours de création.

        Intent idetlocalisation;
        idetlocalisation = getIntent();
        id = idetlocalisation.getIntExtra("id",0);
        lieu = new Vector<Double>();
        lieu.add(idetlocalisation.getDoubleExtra("latitude",0));
        lieu.add(idetlocalisation.getDoubleExtra("longitude",0));



    }

    //onClick pour le bouton "créer évenement", permet d'accéder au menu principal.
    public void versActiPrincipale(){
        Intent nouvelleActi = new Intent();
        nouvelleActi.setClass(this,MenuPrincipalActivity.class);
        nouvelleActi.putExtra("id",id);
        startActivity(nouvelleActi);
        Toast.makeText(this,"Evénement créé !",Toast.LENGTH_SHORT).show();
        finish();
    }

    //Méthode permettant d'ajouter un évènement dans la base de donnée.
    public void creerEvenement(View view){
        EditText Edannee =findViewById(R.id.annee);
        EditText Edmois =findViewById(R.id.mois);
        EditText Edjour =findViewById(R.id.jour);

        description = findViewById(R.id.description).toString();
        listeParticipant = new Vector<Integer>();
        listeParticipant.add(0,id);
        Log.i("datequimerde",String.valueOf(Integer.valueOf(Edannee.getText().toString())-1900));

        date = new Date();
        date.setYear(Integer.valueOf(Edannee.getText().toString())-1900);
        date.setMonth(Integer.valueOf(Edmois.getText().toString())-1 );
        date.setDate(Integer.valueOf(Edjour.getText().toString()));

        SeekBar seek = findViewById(R.id.seekBar);
        impNetoy = seek.getProgress();
        EditText nb = findViewById(R.id.nombrePersonnes);
        nbrPartSouhaité = nb.getInputType();

        Evenement evenement = new Evenement(lieu,100,date,impNetoy,id,listeParticipant,nbrPartSouhaité,description);
        Vector<Evenement> listeEv = getListEvFromLocal();
        if (listeEv == null){
            listeEv =new  Vector<Evenement>();
        }
        listeEv.add(evenement);
        saveListEvenementInLocal(listeEv);
        versActiPrincipale();
    }
    public Vector<Evenement> getListEvFromLocal() {

        SharedPreferences prefs = getSharedPreferences("AppName", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString("liste des évenements", null);
        Type type = new TypeToken<Vector<Evenement>>() {}.getType();
        return gson.fromJson(json, type);
    }

    //Méthode permettant d'inclure une liste d'évènements dans la base de données.
    public void saveListEvenementInLocal(Vector<Evenement> list) {
        SharedPreferences prefs = getSharedPreferences("AppName", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString("liste des évenements", json);
        editor.apply();

    }
}
