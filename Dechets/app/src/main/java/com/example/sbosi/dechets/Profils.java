package com.example.sbosi.dechets;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

public class Profils {

    private Vector<Personne> ensemblePersonnes;

    public Profils() {

        // On créé un ensemble de personnes initial
        ensemblePersonnes = new Vector<Personne>();

        Personne PaulYeme = new Personne(1, "paul.yeme@student-cs.fr", "Yeme", "Paul", 6, "jesuispaul");
        Personne RaphaelBosi = new Personne(2, "raphael.bosi@student-cs.fr", "Bosi", "Raphaël", 9, "jesuisraphael");
        Personne SylvainMuller = new Personne(3, "sylvain.muller@student-cs.fr", "Muller", "Sylvain", 5, "jesuissylvain");
        Personne ZinebLahrichi = new Personne(4, "zineb.lahrichi@student-cs.fr", "Lahrichi", "Zineb", 2, "jesuislebonnet");
        Personne Anonyme = new Personne(5,"","","",4,"");
        ensemblePersonnes.add(PaulYeme);
        ensemblePersonnes.add(RaphaelBosi);
        ensemblePersonnes.add(SylvainMuller);
        ensemblePersonnes.add(ZinebLahrichi);
        ensemblePersonnes.add(Anonyme);
    }
        // retourne l'ensemble des profils des gens sous forme de vecteur de presonnes
        public Vector<Personne> getEnsemblePersonnes(){
            return ensemblePersonnes;
        }

        // retourne le nombre de personnes inscrites
        public int getNombrePersonnes(){
            return ensemblePersonnes.size();
        }

        // ajoute une personne
        public void addPersonne(Personne personne){
            ensemblePersonnes.add(personne);
        }

        //création d'un itérateur de la liste Personne
        public Iterator<Personne> getPersIterator() { return ensemblePersonnes.iterator();}

    }




