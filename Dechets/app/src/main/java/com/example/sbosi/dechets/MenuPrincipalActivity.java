package com.example.sbosi.dechets;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;
import java.util.List;
import java.util.Locale;

public class MenuPrincipalActivity extends AppCompatActivity implements View.OnClickListener {
    private int id;
    int IdEv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        //Récupération de l'identifiant de l'utilisateur après la connexion
        Intent messagedeConnectionInscription = getIntent();
        id = messagedeConnectionInscription.getIntExtra("id", 0);


        //création d'un linear layout pour afficher les évènements.
        LinearLayout ll = findViewById(R.id.linScroll);

        //Création ou récupération de la liste d'évènements
        Vector<Evenement> lesEv = getListEvFromLocal();

        if (lesEv ==null){
            lesEv = new Vector<Evenement>();

            Vector<Double> lieux = new Vector<Double>();
            lieux.add(0,0.1);
            lieux.add(1,0.1);
            int idEv = 0;
            Date dateheure = new Date(119,6,15,10,05);
            int impNetoy = 3;
            int idCreator = 5;
            Vector<Integer> listeParticipant= new Vector<Integer>();
            listeParticipant.add(2);
            int nbrPartSouhaité = 5;
            String description = "description de l'evenement";

            lesEv.add(new Evenement(lieux,idEv,dateheure,impNetoy,idCreator,listeParticipant,nbrPartSouhaité,description));
            saveListEvenementInLocal(lesEv);

        }

        Log.i("sizeEv",lesEv.toString());


        try {

            Iterator<Evenement> it = lesEv.iterator();
            Evenement evenmentEnCours;
            DateFormat dformat = new SimpleDateFormat("yyyy-MM-dd");


            while (it.hasNext()) {
                evenmentEnCours = it.next();
                //pour avoir la date de l'evenement
                Date dateEv = evenmentEnCours.getDate();
                Date dateAuj = new Date();
                String dateAujstring = dformat.format(dateAuj);
                String dateEvstring = dformat.format(dateEv);
                IdEv= evenmentEnCours.getIdEv();




                Button b = new Button(this);
                b.setText("Collecte le " + dateEvstring + " à" + evenmentEnCours.getLieux().get(0).toString() + "lat "+ evenmentEnCours.getLieux().get(1).toString() + "long");
                b.setOnClickListener(this);
                ll.addView(b);


            }
        }
        catch (NullPointerException e) {
            Toast.makeText(this,"Pas de nouvel événement",Toast.LENGTH_SHORT).show();
        }


    }

    //onClick du bouton "création évènement" permettant d'accéder à l'activité Carte
    public void VersCarte(View view) {
        Intent VersCreationactivity = new Intent();
        VersCreationactivity.setClass(this, Carte.class);
        VersCreationactivity.putExtra("id", id);
        startActivity(VersCreationactivity);
    }

    //onClick du bouton "Mon Profil" permettant d'accéder à l'activité MonProfil
    public void VersMonProfil(View view) {
        Intent VersMonprofilactivity = new Intent();
        VersMonprofilactivity.setClass(this, MonProfilActivity.class);
        VersMonprofilactivity.putExtra("id", id);
        startActivity(VersMonprofilactivity);

    }

    //onClick pour les boutons des évènements permettant d'accéder à aux activités AfficherEv
    @Override
    public void onClick(View v) {
        Intent VersEvenement = new Intent();
        VersEvenement.setClass(this, AfficherEvActivity.class);
        VersEvenement.putExtra("id", id);
        VersEvenement.putExtra("idEv",IdEv);
        startActivity(VersEvenement);
    }


    //Méthode permettant de récupérer la liste des évènements déjà répertoriés
    public Vector<Evenement> getListEvFromLocal() {
        SharedPreferences prefs = getSharedPreferences("AppName", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString("liste des évenements", null);
        Type type = new TypeToken<Vector<Evenement>>() {}.getType();
        return gson.fromJson(json, type);
    }

    //Méthode permettant d'inclure la liste des évènements dans la base de données.
    public void saveListEvenementInLocal(Vector<Evenement> list) {

        SharedPreferences prefs = getSharedPreferences("AppName", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString("liste des évenements", json);
        editor.apply();

    }
}
