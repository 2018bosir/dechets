package com.example.sbosi.dechets;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Vector;

public class MonProfilActivity extends AppCompatActivity {
    Personne PersonneConnectée;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mon_profil);


        //On récupère l'identifiant de l'utilisateur depuis le menu principal
        Intent messageMenuPrincipal = getIntent();
        int id = messageMenuPrincipal.getIntExtra("id",0);

        Vector<Personne> listePers = getListPersonneFromLocal();
        Iterator<Personne> it = listePers.iterator();


        while(it.hasNext()) {
            Personne personne = it.next();
            if (id == personne.getIdPersonne()) {
                PersonneConnectée = personne;
                break;
            }
        }

        //On peut afficher les informations de l'utilisateur
        TextView TVnom = findViewById(R.id.textView2);
        TextView TVprenom = findViewById(R.id.textView3);
        TextView TVemail = findViewById(R.id.textView4);

        TVnom.setText(PersonneConnectée.getNom());
        TVprenom.setText(PersonneConnectée.getPrenom());
        TVemail.setText(PersonneConnectée.getMail());
        RatingBar rb =findViewById(R.id.ratingBar3);
        rb.setRating(PersonneConnectée.getPoints());

    }
    public Vector<Personne> getListPersonneFromLocal() {
        SharedPreferences prefs = getSharedPreferences("AppName", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString("liste personnes enregistré", null);
        Type type = new TypeToken<Vector<Personne>>() {}.getType();
        return gson.fromJson(json, type);

    }

}
