package com.example.sbosi.dechets;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.MapView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import java.util.Vector;

public class AfficherEvActivity extends AppCompatActivity {
    private int id;
    private int IdEv;
    Evenement evenementAffiché;
    Personne participant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_afficher_ev);

        //On récupère l'identifiant de la personne connectée à l'application.
        Intent messagedeConnectionInscription = getIntent();
        id = messagedeConnectionInscription.getIntExtra("id", 0);

        //On récupère l'identifiant de l'évenement sur lequel on a cliqué.
        Intent messagedeMenu = getIntent();
        IdEv = messagedeMenu.getIntExtra("idEv", 0);





        //On récupère la base de données.
        Vector<Evenement> listeEv = getListEvFromLocal();
        Vector<Personne> listePersonnes= getListPersonneFromLocal();
        Iterator<Evenement> it = listeEv.iterator();


        // On récupère l'évenement dans la base de données grace à son ID
        while (it.hasNext()) {
            Evenement eV = it.next();
            if (IdEv == eV.getIdEv()) {
                evenementAffiché = eV;
                break;
            }
        }


        //Maintenant qu'on a l'évenement on peut récupérer les informations qui lui sont liées et on les affiche (date, description..)
        TextView date = findViewById(R.id.date);
        DateFormat dformat = new SimpleDateFormat("yyyy-MM-dd");

        Date datedelev;
        datedelev =evenementAffiché.getDate();
        String dateEvenementAffiché = dformat.format(datedelev);
        date.setText(dateEvenementAffiché);


        /*Affichage de la liste des participants sur un scrollView.
        On récupère la liste des id des participants de l'évenement,
        ensuite on cherche les personnes associées dans la base de donnée à l'aide d'un iterateur
        0n affiche leur nom, prénom et mail.*/

        //Création de la liste de participants
        LinearLayout Monlayout = findViewById(R.id.Monlayout);

        for(int k = 0; k< evenementAffiché.getListeParticipant().size(); k++){
            Button b = new Button(this);
            int Idparticipant =evenementAffiché.getListeParticipant().get(k);
            //On cherche la personne associée à l'ID (création d'un itérateur)
            Iterator<Personne> iterator = listePersonnes.iterator();
            while (iterator.hasNext()){
                Personne P=iterator.next();
                if (P.getIdPersonne()== Idparticipant){
                    participant = P;
                }
            }
            b.setText(participant.getPrenom() + participant.getNom() + " mail: " + participant.getMail());
            Monlayout.addView(b);
            }
        }




    /*onClick du bouton inscription.
    Rajoute l'id de la personne connectée dans le vecteur listeParticipant si elle ne s'est pas déjà inscrite.*/
    public void inscrire(View view) {
        Vector<Evenement> listeEv = getListEvFromLocal();
        if (evenementAffiché.getListeParticipant().contains(id)) {
            Toast.makeText(this, "Vous participez déjà à l'évenement", Toast.LENGTH_SHORT).show();
        }
        else {
            evenementAffiché.getListeParticipant().add(id);
            Toast.makeText(this, "inscription réussie!", Toast.LENGTH_SHORT).show();
        }
        Iterator<Evenement> it = listeEv.iterator();
        Evenement ev ;
        Vector<Evenement> listenew = new Vector<Evenement>();
        while (it.hasNext()){
            ev = it.next();
            if (evenementAffiché.getIdEv() != ev.getIdEv()){
                listenew.add(ev);
            }
        }
        listenew.add(evenementAffiché);
        saveListEvenementInLocal(listenew);

    }

    //Création d'un sharedPreference pour stocker la liste des personnes inscrites
    public Vector<Evenement> getListEvFromLocal() {
        SharedPreferences prefs = getSharedPreferences("AppName", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString("liste des évenements", null);
        Type type = new TypeToken<Vector<Evenement>>() {}.getType();
        return gson.fromJson(json, type);
    }


    //Création d'un sharedPreference pour stocker la liste des personnes et celle des évènements créés
    public Vector<Personne> getListPersonneFromLocal() {
        SharedPreferences prefs = getSharedPreferences("AppName", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString("liste personnes enregistré", null);
        Type type = new TypeToken<Vector<Personne>>() {}.getType();
        return gson.fromJson(json, type);

    }
    public void saveListEvenementInLocal(Vector<Evenement> list) {
        SharedPreferences prefs = getSharedPreferences("AppName", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString("liste des évenements", json);
        editor.apply();

    }

    public void localisation(View view) {
        Intent messagecarte = new Intent();
        Double lat = evenementAffiché.getLieux().get(0);
        Double lon = evenementAffiché.getLieux().get(1);
        messagecarte.setClass(this,Carte2.class);
        messagecarte.putExtra("lat",lat);
        messagecarte.putExtra("lon",lon);
        startActivity(messagecarte);
    }
}
