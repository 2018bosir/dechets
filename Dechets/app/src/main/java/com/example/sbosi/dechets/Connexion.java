package com.example.sbosi.dechets;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Vector;

public class Connexion extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);
    }

    private int id;
    public void connexion(View view)
    {
        //affectation des variables
        EditText mail = findViewById(R.id.mail);
        EditText mdp = findViewById(R.id.mdp);
        Vector<Personne> profils = getListPersonneFromLocal();
        if (profils == null ){
            profils =new Vector<Personne>();
            Personne PaulYeme = new Personne(1, "paul.yeme@student-cs.fr", "Yeme", "Paul", 6, "jesuispaul");
            Personne RaphaelBosi = new Personne(2, "raphael.bosi@student-cs.fr", "Bosi", "Raphaël", 9, "jesuisraphael");
            Personne SylvainMuller = new Personne(3, "sylvain.muller@student-cs.fr", "Muller", "Sylvain", 5, "jesuissylvain");
            Personne ZinebLahrichi = new Personne(4, "zineb.lahrichi@student-cs.fr", "Lahrichi", "Zineb", 2, "jesuislebonnet");
            Personne Anonyme = new Personne(5,"","","",4,"");
            profils.add(PaulYeme);
            profils.add(RaphaelBosi);
            profils.add(SylvainMuller);
            profils.add(ZinebLahrichi);
            profils.add(Anonyme);
        }
        saveListPersInLocal(profils);
        Boolean connec=false;
        // création d'un itérateur pour parcourir le vecteur de personnes
        Iterator<Personne> it = profils.iterator();

        //on teste si les identifiants et mot de passe sont corrects, si oui on passe au menu pricipal
        while(it.hasNext()){
            Personne personne = it.next();
            if(mail.getText().toString().equals(personne.getMail())){
                if(mdp.getText().toString().equals(personne.getMotDePasse())){
                    connec=true;
                    id = personne.getIdPersonne();
                    Toast.makeText(this, "Connexion Réussie", Toast.LENGTH_SHORT).show();
                    changeActi();
                }
            }

        }
        if (!connec){
        Toast.makeText(this, "Identifiant ou mot de passe incorrect", Toast.LENGTH_SHORT).show();}
    }


    //onClik du bouton "se connecter", permet d'accéder au menu principal et d'envoyer l'id de la personne connectée
    public void changeActi()
    {
        Intent nouvelleActi = new Intent();
        nouvelleActi.setClass(this,MenuPrincipalActivity.class);
        nouvelleActi.putExtra("id",id);
        startActivity(nouvelleActi);
        finish();
    }

    //Méthode permettant de récupérer la liste des personnes déjà inscrites.
    public Vector<Personne> getListPersonneFromLocal()
    {
        SharedPreferences prefs = getSharedPreferences("AppName", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString("liste personnes enregistré", null);
        Type type = new TypeToken<Vector<Personne>>() {}.getType();
        return gson.fromJson(json, type);

    }
    public void saveListPersInLocal(Vector<Personne> list) {

        SharedPreferences prefs = getSharedPreferences("AppName", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString("liste personnes enregistré", json);
        editor.apply();     // This line is IMPORTANT !!!

    }
}

