package com.example.sbosi.dechets;

import android.location.Address;
import android.location.Geocoder;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import java.sql.Time;
import java.util.Date;

public class Evenement {
    private Vector<Double> lieux;
    private int idEv;
    private Date dateheure;
    private int impNetoy;  //entier de 1 a 3
    private int idCreator;
    private Vector<Integer> listeParticipant;
    private int nbrPartSouhaité;
    private String description;

    public Evenement(Vector<Double> lieux0,int idEv0, Date date0, int impNetoy0, int idCreator0, Vector<Integer> listeParticipant0, Integer nbrPartSouhaité0, String description0) {
        lieux = lieux0;
        idEv = idEv0;
        dateheure = date0;
        impNetoy = impNetoy0;
        idCreator = idCreator0;
        listeParticipant = listeParticipant0;
        nbrPartSouhaité = nbrPartSouhaité0;
        description = description0;
    }


    //retourne un vecteur contenant la latitude et la longitude (coordonnées GPS du lieu de l'évènement
    public Vector<Double> getLieux() {
        return lieux;
    }

    //retourne la date de l'évènement
    public Date getDate() { // [jour,mois,année,heuredebut]
        return dateheure;
    }

    //retourne l'ID du créateur de l'évènement
    public int getIdCreator() {
        return idCreator;
    }

    //retourne l'importance du nettoyage exprimée en une note allant de 1 à 3
    public int getImpNetoy() {
        return impNetoy;
    }

    //retourne le nombre de participants souhaités par le créateur
    public Integer getNbrPartSouhaité() {
        return nbrPartSouhaité;
    }

    //retourne la liste des ID des participants
    public Vector<Integer> getListeParticipant() {
        return listeParticipant;
    }

    //retourne l'ID de l'évènement
    public int getIdEv(){return idEv;}

    //retourne la description laissée par le créateur
    public String getDescription(){return description;}


}
