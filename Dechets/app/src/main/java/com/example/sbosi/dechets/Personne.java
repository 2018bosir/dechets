package com.example.sbosi.dechets;

public class Personne {
    private int idPersonne;
    private String mail;
    private String nom;
    private String prenom;
    private int points;
    private String motDePasse;

    public Personne(int idPersonne0, String mail0, String nom0, String prenom0, int points0, String motDePasse0){
        idPersonne=idPersonne0;
        mail=mail0;
        nom=nom0;
        prenom=prenom0;
        points=points0;
        motDePasse=motDePasse0;
    }

    public int getIdPersonne() {
        return idPersonne;
    }

    public String getMail() {
        return mail;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public int getPoints() {
        return points;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public String getPrenomNom() {
        return prenom+" "+nom;
    }

}
