package com.example.sbosi.dechets;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Vector;

public class InscriptionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
    }


    /*Méthode permettant de récupérer les informations apportées par l'utilisateur
    * Si la personne ne s'est pas déjà inscrite, on l'ajoute à la liste des personnes déjà inscrites.
    */
    public void validéInscription(View view) {
        EditText ednom = findViewById(R.id.editTextNom);
        EditText edprenom = findViewById(R.id.editTextPrenom);
        EditText edmail = findViewById(R.id.editTextEmail);
        EditText edmdp = findViewById(R.id.editTextMdp);

        String nom = ednom.getText().toString();
        String prenom = edprenom.getText().toString();
        String email = edmail.getText().toString();
        String mdp = edmdp.getText().toString();

        Vector<Personne> liste = getListPersonneFromLocal();
        if (liste == null){
            liste = new Vector<Personne>();
            Personne PaulYeme = new Personne(1, "paul.yeme@student-cs.fr", "Yeme", "Paul", 6, "jesuispaul");
            Personne RaphaelBosi = new Personne(2, "raphael.bosi@student-cs.fr", "Bosi", "Raphaël", 9, "jesuisraphael");
            Personne SylvainMuller = new Personne(3, "sylvain.muller@student-cs.fr", "Muller", "Sylvain", 5, "jesuissylvain");
            Personne ZinebLahrichi = new Personne(4, "zineb.lahrichi@student-cs.fr", "Lahrichi", "Zineb", 2, "jesuislebonnet");
            Personne Anonyme = new Personne(5,"","","",4,"");
            liste.add(PaulYeme);
            liste.add(RaphaelBosi);
            liste.add(SylvainMuller);
            liste.add(ZinebLahrichi);
            liste.add(Anonyme);
        }
        saveListPersInLocal(liste);

        //On affecte au nouvel utilisateur un identifiant qui vaut celui du dernier inscrit plus un.

        if (!nom.equals("") && !prenom.equals("") && !email.equals("") && !mdp.equals("")) {
            int id = liste.size() + 1;
            Personne pers = new Personne(id, email, nom, prenom, 0, mdp);
            liste.add(pers);

            saveListPersInLocal(liste);


            //On envoie l'identifiant de l'utilisateur à la prochaine activité.
            Intent messageVersMenuPrincipal = new Intent();
            messageVersMenuPrincipal.setClass(this, MenuPrincipalActivity.class);
            messageVersMenuPrincipal.putExtra("id",id);
            startActivity(messageVersMenuPrincipal);
            finish();
            Toast.makeText(this, "Bravo ! Inscription réussit", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "Champs manquant ! Inscription refusée !", Toast.LENGTH_SHORT).show();
        }
    }

    //Méthode permettant de récupérer la liste des personnes déjà inscrites.
    public Vector<Personne> getListPersonneFromLocal() {
        SharedPreferences prefs = getSharedPreferences("AppName", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString("liste personnes enregistré", null);
        Type type = new TypeToken<Vector<Personne>>() {}.getType();
        return gson.fromJson(json, type);
    }

    //Méthode permettant d'inclure la liste des personnes inscrites dans la base de données.
    public void saveListPersInLocal(Vector<Personne> list) {

        SharedPreferences prefs = getSharedPreferences("AppName", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString("liste personnes enregistré", json);
        editor.apply();

    }
}
