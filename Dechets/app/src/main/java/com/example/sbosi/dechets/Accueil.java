package com.example.sbosi.dechets;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Intent;
import android.view.View;

/*Cette classe gère l'accueil de l'application. Elle permet en affichant deux boutons soit de se connecter soit de s'inscrire.
 */

public class Accueil extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
    }

    //onClick pour le bouton connexion (dirige vers l'activité connexion)
    public void allerPageConnexion(View view) {
        Intent messageVersConnexion = new Intent();
        messageVersConnexion.setClass(this,Connexion.class);
        startActivity(messageVersConnexion);
    }

    //onClick pour le bouton connexion (dirige vers l'activité inscription)
    public void allerPageInscription(View view) {
        Intent messageVersInscription = new Intent();
        messageVersInscription.setClass(this,InscriptionActivity.class);
        startActivity(messageVersInscription);
    }
}

